package it.com.atlassianlab.bamboo.plugins.afterdeploymenttrigger;

import com.atlassian.bamboo.pageobjects.BambooTestedProduct;
import com.atlassian.bamboo.pageobjects.BambooTestedProductFactory;
import com.atlassian.bamboo.pageobjects.pages.deployment.EnvironmentSummaryPage;
import com.atlassian.bamboo.pageobjects.pages.deployment.execute.ExecuteDeploymentPage;
import com.atlassian.bamboo.pageobjects.pages.plan.configuration.ConfigurePlanTriggersPage;
import com.atlassian.bamboo.testutils.TestBuildDetailsBuilder;
import com.atlassian.bamboo.testutils.UniqueNameHelper;
import com.atlassian.bamboo.testutils.junit.rule.BackdoorRule;
import com.atlassian.bamboo.testutils.model.TestBuildDetails;
import com.atlassian.bamboo.testutils.model.deployments.environments.TestEnvironmentDetails;
import com.atlassian.bamboo.testutils.model.deployments.projects.TestDeploymentProjectDetails;
import com.atlassian.bamboo.testutils.model.task.ScriptTaskDetails;
import com.atlassian.bamboo.testutils.user.TestUser;
import com.atlassian.bamboo.webdriver.WebDriverTestEnvironmentData;
import com.atlassianlab.bamboo.plugins.afterdeploymenttrigger.pageobject.AfterDeploymentTriggerComponent;
import org.jetbrains.annotations.NotNull;
import org.junit.Rule;
import org.junit.Test;

import javax.annotation.Nullable;

public class DeploymentTriggerTest
{
    private final WebDriverTestEnvironmentData environmentData = new WebDriverTestEnvironmentData();
    private final BambooTestedProduct bamboo = BambooTestedProductFactory.create();

    @Rule public final BackdoorRule backdoor = new BackdoorRule(environmentData);

    @Test
    public void planIsTriggeredAfterDeploymentComplete() throws Exception
    {
        //create plan 1 with artifacts
        final TestBuildDetails parent = new TestBuildDetailsBuilder()
                .withArtifact(builder -> builder.withName("text").withCopyPattern("*.txt").build())
                .withScriptTask("echo 5 > text.txt")
                .withName(UniqueNameHelper.makeUniqueName("parent"))
                .build();
        backdoor.plans().createPlan(parent);
        //create deployment with 2 environments
        final TestDeploymentProjectDetails deploymentProjectDetails = createDeployment(parent);
        //create plan 2
        final TestBuildDetails child = new TestBuildDetailsBuilder()
                .withScriptTask()
                .withNoInitialBuild()
                .build();
        backdoor.plans().createPlan(child);
        //add After deployment trigger to plan 2
        final TestEnvironmentDetails environment = deploymentProjectDetails.getEnvironments().get(0);
        bamboo.fastLogin(TestUser.ADMIN);
        addDependency(child, deploymentProjectDetails, environment);
        //trigger QA environment
        ExecuteDeploymentPage executeDeploymentPage = bamboo.visit(EnvironmentSummaryPage.class, environment.getId()).deploy();
        final String releaseName = UniqueNameHelper.makeUniqueName("release");
        executeDeploymentPage.withCreateNewRelease()
                .withReleaseName(releaseName)
                .done()
                .submit();

        backdoor.deployments().environments().waitForCompletedDeployment(environment.getId(), releaseName);

        //confirm child plan was triggered
        backdoor.plans().waitForFinishedBuild(child.getKey(), 1);
    }

    @NotNull
    private TestDeploymentProjectDetails createDeployment(TestBuildDetails parent)
    {
        final TestDeploymentProjectDetails deploymentProjectDetails = new TestDeploymentProjectDetails.Builder()
                .withName(UniqueNameHelper.makeUniqueName("deploy"))
                .withPlan(parent.getKey())
                .addEnvironment(envBuilder -> envBuilder.name("QA").task(ScriptTaskDetails.builder().scriptBody("echo \"QA\"").build()).build())
                .addEnvironment(envBuilder -> envBuilder.name("Staging").task(ScriptTaskDetails.builder().scriptBody("echo \"Staging\"").build()).build())
                .build();
        backdoor.deployments().createDeploymentProject(deploymentProjectDetails);
        return deploymentProjectDetails;
    }

    private void addDependency(TestBuildDetails plan,
                               TestDeploymentProjectDetails deployment,
                               @Nullable TestEnvironmentDetails environment)
    {
        final ConfigurePlanTriggersPage triggersPage = bamboo.visit(ConfigurePlanTriggersPage.class, plan.getKey());
        final AfterDeploymentTriggerComponent triggerComponent = triggersPage.addTrigger("After deployment",
                AfterDeploymentTriggerComponent.class);
        triggerComponent.setDeploymentProject(deployment);
        triggerComponent.setEnvironment(environment);
        triggerComponent.save();
    }
}
