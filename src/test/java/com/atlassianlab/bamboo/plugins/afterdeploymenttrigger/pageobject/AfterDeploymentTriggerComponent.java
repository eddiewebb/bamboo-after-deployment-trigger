package com.atlassianlab.bamboo.plugins.afterdeploymenttrigger.pageobject;

import com.atlassian.bamboo.pageobjects.elements.Select2Element;
import com.atlassian.bamboo.pageobjects.pages.triggers.AbstractTriggerComponent;
import com.atlassian.bamboo.testutils.model.deployments.environments.TestEnvironmentDetails;
import com.atlassian.bamboo.testutils.model.deployments.projects.TestDeploymentProjectDetails;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.webdriver.AtlassianWebDriver;

import javax.annotation.Nullable;

public class AfterDeploymentTriggerComponent extends AbstractTriggerComponent
{
    @ElementBy(id = "deploymentProject")
    private Select2Element deployments;
    @ElementBy(id = "deploymentEnvironment")
    private Select2Element environments;

    public AfterDeploymentTriggerComponent(@SuppressWarnings("deprecation") final AtlassianWebDriver driver,
                                           final String formName)
    {
        super(driver, formName);
    }

    public void setDeploymentProject(TestDeploymentProjectDetails deployment)
    {
        deployments.enterValue(deployment.getName());
    }

    public void setEnvironment(@Nullable TestEnvironmentDetails environment)
    {
        if (environment == null)
        {
            environments.enterValue("Any");
        }
        else
        {
            environments.enterValue(environment.getName());
        }
    }
}
