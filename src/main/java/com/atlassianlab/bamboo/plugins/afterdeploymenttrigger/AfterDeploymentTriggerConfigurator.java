package com.atlassianlab.bamboo.plugins.afterdeploymenttrigger;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.deployments.environments.Environment;
import com.atlassian.bamboo.deployments.environments.service.EnvironmentService;
import com.atlassian.bamboo.deployments.environments.trigger.EnvironmentAwareEnvironmentTriggerConfigurator;
import com.atlassian.bamboo.deployments.projects.DeploymentProject;
import com.atlassian.bamboo.deployments.projects.service.DeploymentProjectService;
import com.atlassian.bamboo.trigger.TriggerDefinition;
import com.atlassian.bamboo.utils.Pair;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.sal.api.message.I18nResolver;
import org.acegisecurity.AccessDeniedException;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class AfterDeploymentTriggerConfigurator implements EnvironmentAwareEnvironmentTriggerConfigurator
{
    private static final Logger log = Logger.getLogger(AfterDeploymentTriggerConfigurator.class);
    static final long ANY_ENVIRONMENT_ID = 0L;
    static final String CFG_DEPLOYMENT_PROJECT_ID = "deployment.trigger.afterDeployment.deploymentProjectId";

    static final String CFG_DEPLOYMENT_ENVIRONMENT_ID = "deployment.trigger.afterDeployment.deploymentEnvironmentId";
    private static final String CFG_DEPLOYMENT_ENVIRONMENTS = "deploymentEnvironments";
    private static final String CFG_DEPLOYMENT_PROJECTS = "deploymentProjects";

    private final DeploymentProjectService deploymentProjectService;
    private final EnvironmentService environmentService;
    private final I18nResolver i18nResolver;

    @Autowired
    public AfterDeploymentTriggerConfigurator(final EnvironmentService environmentService,
                                              final I18nResolver i18nResolver,
                                              final DeploymentProjectService deploymentProjectService)
    {
        this.environmentService = environmentService;
        this.i18nResolver = i18nResolver;
        this.deploymentProjectService = deploymentProjectService;
    }

    @Override
    public boolean isAffectedByEnvironmentDeletion(long environmentId, @NotNull TriggerDefinition triggerDefinition)
    {
        final Map<String, String> configuration = triggerDefinition.getConfiguration();
        return configuration.getOrDefault(CFG_DEPLOYMENT_ENVIRONMENT_ID, String.valueOf(ANY_ENVIRONMENT_ID)).equals(String.valueOf(environmentId));
    }

    @Override
    public void populateContextForCreate(@NotNull Map<String, Object> context)
    {
        populateContextForAllOperations(context);
    }

    @Override
    public void populateContextForEdit(@NotNull Map<String, Object> context, @NotNull TriggerDefinition triggerDefinition)
    {
        prepareConfig(context, triggerDefinition);
        populateContextForAllOperations(context);
        final String deploymentProjectIdCfg = triggerDefinition.getConfiguration().get(CFG_DEPLOYMENT_PROJECT_ID);
        if (NumberUtils.isNumber(deploymentProjectIdCfg))
        {
            Long deploymentProjectId = Long.valueOf(deploymentProjectIdCfg);
            DeploymentProject deploymentProject = deploymentProjectService.getDeploymentProject(deploymentProjectId);
            if (deploymentProject != null)
            {
                final List<? extends Environment> environments = deploymentProject.getEnvironments();
                putEnvironmentsToContext(context, environments);
            }
            else
            {
                log.warn("Deployment project is null for " + deploymentProjectId);
            }
        }
    }

    @Override
    public void populateContextForView(@NotNull Map<String, Object> context, @NotNull TriggerDefinition triggerDefinition)
    {
        prepareConfig(context, triggerDefinition);
        populateContextForAllOperations(context);
    }

    @Override
    public void validate(@NotNull ActionParametersMap actionParametersMap, @NotNull ErrorCollection errorCollection)
    {
        validateDeploymentProject(actionParametersMap, errorCollection);
        validateEnvironment(actionParametersMap, errorCollection);
    }

    @NotNull
    @Override
    public Map<String, String> generateTriggerConfigMap(@NotNull ActionParametersMap actionParametersMap, @Nullable TriggerDefinition previousTriggerDefinition)
    {
        final Map<String, String> cfgMap = new HashMap<>();
        cfgMap.put(CFG_DEPLOYMENT_PROJECT_ID, actionParametersMap.getString(CFG_DEPLOYMENT_PROJECT_ID));
        cfgMap.put(CFG_DEPLOYMENT_ENVIRONMENT_ID, actionParametersMap.getString(CFG_DEPLOYMENT_ENVIRONMENT_ID));
        return cfgMap;
    }

    @NotNull
    @Override
    public RepositorySelectionMode getRepositorySelectionMode()
    {
        return RepositorySelectionMode.NONE;
    }

    private void validateDeploymentProject(@NotNull ActionParametersMap actionParametersMap, @NotNull ErrorCollection errorCollection)
    {
        final long deploymentProjectId = actionParametersMap.getLong(CFG_DEPLOYMENT_PROJECT_ID, 0);
        if (deploymentProjectId == 0)
        {
            errorCollection.addError(CFG_DEPLOYMENT_PROJECT_ID, i18nResolver.getText("deployment.trigger.afterDeployment.deploymentProject.error.required"));
        }
        else if (deploymentProjectId > 0)
        {
            try
            {
                final DeploymentProject deploymentProject = deploymentProjectService.getDeploymentProject(deploymentProjectId);
                if (deploymentProject == null)
                {
                    errorCollection.addError(CFG_DEPLOYMENT_PROJECT_ID, i18nResolver.getText("deployment.trigger.afterDeployment.deploymentProject.not.found"));
                }
            }
            catch (AccessDeniedException e)
            {
                errorCollection.addError(CFG_DEPLOYMENT_PROJECT_ID, i18nResolver.getText("deployment.trigger.afterDeployment.deploymentProject.no.permissions"));
            }
        }
    }

    private void validateEnvironment(@NotNull ActionParametersMap actionParametersMap, @NotNull ErrorCollection errorCollection)
    {
        final long environmentId = actionParametersMap.getLong(CFG_DEPLOYMENT_ENVIRONMENT_ID, 0);
        if (environmentId == -1)
        {
            errorCollection.addError(CFG_DEPLOYMENT_ENVIRONMENT_ID, i18nResolver.getText("deployment.trigger.afterDeployment.deploymentEnvironment.error.required"));
        }
        else if (environmentId > 0)
        {
            try
            {
                final Environment environment = environmentService.getEnvironment(environmentId);
                if (environment == null)
                {
                    errorCollection.addError(CFG_DEPLOYMENT_ENVIRONMENT_ID, i18nResolver.getText("deployment.trigger.afterDeployment.deploymentEnvironment.not.found"));
                }
            }
            catch (AccessDeniedException e)
            {
                errorCollection.addError(CFG_DEPLOYMENT_ENVIRONMENT_ID, i18nResolver.getText("deployment.trigger.afterDeployment.deploymentEnvironment.error.no.permisions"));
            }
        }
    }

    private void populateContextForAllOperations(@NotNull Map<String, Object> context)
    {
        final List<DeploymentProject> deploymentProjects = deploymentProjectService.getAllDeploymentProjects();
        final List<Pair<String, Long>> deployments = deploymentProjects.stream()
                .filter(deploymentProject -> deploymentProject.getOperations().isCanView())
                .map(deployment -> Pair.make(deployment.getName(), deployment.getId()))
                .collect(Collectors.toCollection(ArrayList::new));
        deployments.add(0, Pair.make(i18nResolver.getText("deployment.trigger.afterDeployment.deploymentProject.choose"), 0L));
        context.put(CFG_DEPLOYMENT_PROJECTS, deployments);

        putEnvironmentsToContext(context, Collections.emptyList());
    }

    private void putEnvironmentsToContext(@NotNull Map<String, Object> context,
                                          @NotNull List<? extends Environment> environments)
    {
        List<Pair<String, Long>> deploymentEnvironments = new ArrayList<>();
        deploymentEnvironments.add(Pair.make("Any", ANY_ENVIRONMENT_ID));
        final List<Pair<String, Long>> preparedEnvironments = environments.stream()
                .map(environment -> Pair.make(environment.getName(), environment.getId()))
                .collect(Collectors.toList());
        deploymentEnvironments.addAll(preparedEnvironments);
        context.put(CFG_DEPLOYMENT_ENVIRONMENTS, deploymentEnvironments);
    }

    private void prepareConfig(@NotNull Map<String, Object> context, @NotNull TriggerDefinition triggerDefinition)
    {
        final Map<String, String> existingConfiguration = triggerDefinition.getConfiguration();
        context.put(CFG_DEPLOYMENT_PROJECT_ID, existingConfiguration.get(CFG_DEPLOYMENT_PROJECT_ID));
        context.put(CFG_DEPLOYMENT_ENVIRONMENT_ID, existingConfiguration.get(CFG_DEPLOYMENT_ENVIRONMENT_ID));
    }
}