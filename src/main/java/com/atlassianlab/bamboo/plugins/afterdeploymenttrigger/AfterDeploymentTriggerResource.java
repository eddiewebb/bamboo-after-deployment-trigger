package com.atlassianlab.bamboo.plugins.afterdeploymenttrigger;

import com.atlassian.bamboo.deployments.projects.DeploymentProject;
import com.atlassian.bamboo.deployments.projects.service.DeploymentProjectService;
import com.atlassian.bamboo.exception.NotFoundException;
import com.atlassian.plugins.rest.common.security.AuthenticationContext;
import com.sun.jersey.spi.resource.Singleton;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Path("afterDeployment")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
@Singleton
public class AfterDeploymentTriggerResource
{
    private final DeploymentProjectService deploymentProjectService;

    @Autowired
    public AfterDeploymentTriggerResource(final DeploymentProjectService deploymentProjectService)
    {
        this.deploymentProjectService = deploymentProjectService;
    }

    @GET
    @Path("{deploymentProjectId}/environments")
    public List<RestEnvironment> findEnvironmentsByDeploymentProject(@PathParam("deploymentProjectId") long deploymentProjectId,
                                                                     @QueryParam("selectedEnvironment") Long selectedEnvironment,
                                                                     @Context final AuthenticationContext authenticationContext)
    {
        final DeploymentProject deploymentProject = deploymentProjectService.getDeploymentProject(deploymentProjectId);
        if(deploymentProject == null)
        {
            throw new NotFoundException("Can't find deployment project by id: " + deploymentProjectId);
        }
        return deploymentProject.getEnvironments().stream()
                .map(RestEnvironment::new)
                .collect(Collectors.toCollection(ArrayList::new));
    }
}
