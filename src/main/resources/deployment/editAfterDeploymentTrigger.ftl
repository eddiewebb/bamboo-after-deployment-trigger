[#-- @ftlvariable name="statuses" type="java.util.List<com.atlassian.bamboo.utils.Pair<String, String>>" --]
[#-- @ftlvariable name="deploymentProjects" type="java.util.List<com.atlassian.bamboo.utils.Pair<String, Long>>" --]
[@s.select id="deploymentProject"
    labelKey='deployment.trigger.afterDeployment.deploymentProject.label'
    name='deployment.trigger.afterDeployment.deploymentProjectId'
    required=true
    list="deploymentProjects"
    listKey='second' listValue='first'
    cssClass='long-field select2-container aui-select2-container'/]

[@s.textfield id="deploymentEnvironment"
    labelKey='deployment.trigger.afterDeployment.environment.label'
    name='deployment.trigger.afterDeployment.deploymentEnvironmentId'
    cssClass='long-field select2-container aui-select2-container'/]

<script type="application/javascript">
    require(['feature/trigger/after-deployment-trigger-app'], function (AfterDeploymentApp) {
        new AfterDeploymentApp({
            deploymentProject: '#deploymentProject',
            deploymentEnvironment: '#deploymentEnvironment'
        }).start();
    });
</script>