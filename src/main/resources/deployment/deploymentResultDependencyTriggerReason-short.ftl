[#if triggerReason.resultIdString??]
Triggered by [@compress singleLine=true]
    [#if baseUrl?has_content]
        <a href="${baseUrl}/deploy/viewDeploymentResult.action?deploymentResultId=${triggerReason.resultIdString}">${triggerReason.deploymentProjectName} &rsaquo; ${triggerReason.environmentName}</a>
    [#else]
        ${triggerReason.deploymentProjectName} &rsaquo; ${triggerReason.environmentName}
    [/#if]
[/@compress]
[#else]
    Triggered by deployment
[/#if]