package tutorial;

import com.atlassian.bamboo.specs.api.BambooSpec;
import com.atlassian.bamboo.specs.api.builders.AtlassianModule;
import com.atlassian.bamboo.specs.api.builders.BambooKey;
import com.atlassian.bamboo.specs.api.builders.credentials.SharedCredentialsIdentifier;
import com.atlassian.bamboo.specs.api.builders.notification.Notification;
import com.atlassian.bamboo.specs.api.builders.plan.Job;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.builders.plan.Stage;
import com.atlassian.bamboo.specs.api.builders.plan.artifact.Artifact;
import com.atlassian.bamboo.specs.api.builders.plan.branches.BranchCleanup;
import com.atlassian.bamboo.specs.api.builders.plan.branches.PlanBranchManagement;
import com.atlassian.bamboo.specs.api.builders.plan.configuration.AllOtherPluginsConfiguration;
import com.atlassian.bamboo.specs.api.builders.plan.configuration.ConcurrentBuilds;
import com.atlassian.bamboo.specs.api.builders.project.Project;
import com.atlassian.bamboo.specs.api.builders.repository.VcsChangeDetection;
import com.atlassian.bamboo.specs.api.builders.repository.VcsRepositoryIdentifier;
import com.atlassian.bamboo.specs.api.builders.requirement.Requirement;
import com.atlassian.bamboo.specs.api.builders.task.AnyTask;
import com.atlassian.bamboo.specs.api.builders.trigger.AnyTrigger;
import com.atlassian.bamboo.specs.api.builders.trigger.RepositoryBasedTrigger;
import com.atlassian.bamboo.specs.builders.notification.PlanStatusChangedNotification;
import com.atlassian.bamboo.specs.builders.notification.UserRecipient;
import com.atlassian.bamboo.specs.builders.repository.bitbucket.cloud.BitbucketCloudRepository;
import com.atlassian.bamboo.specs.builders.repository.viewer.BitbucketCloudRepositoryViewer;
import com.atlassian.bamboo.specs.builders.task.CheckoutItem;
import com.atlassian.bamboo.specs.builders.task.MavenTask;
import com.atlassian.bamboo.specs.builders.task.ScriptTask;
import com.atlassian.bamboo.specs.builders.task.VcsCheckoutTask;
import com.atlassian.bamboo.specs.builders.trigger.RepositoryPollingTrigger;
import com.atlassian.bamboo.specs.util.BambooServer;
import com.atlassian.bamboo.specs.util.MapBuilder;
import java.time.Duration;

@BambooSpec
public class PlanSpec {

    private Plan plan() {
        return new Plan(new Project()
                .key(new BambooKey("ZA")),
                "After deployment trigger plugin",
                new BambooKey("ADT"))
                .description("https://marketplace.atlassian.com/plugins/com.atlassianlab.bamboo.plugins.bamboo-after-deployment-trigger-plugin/server/overview")
                .pluginConfigurations(new ConcurrentBuilds()
                                .useSystemWideDefault(false)
                                .maximumNumberOfConcurrentBuilds(10),
                        new AllOtherPluginsConfiguration()
                                .configuration(new MapBuilder<String, Object>()
                                        .put("custom.com.atlassian.bamboo.plugin.hungbuildkiller.stopped.enabled", "true")
                                        .build()))
                .stages(new Stage("Default Stage")
                        .jobs(new Job("Default Job",
                                new BambooKey("JOB1"))
                                .artifacts(new Artifact()
                                        .name("Logs")
                                        .copyPattern("target/**/*.logs")
                                        .location("plugin"))
                                .requirements(new Requirement("os")
                                        .matchType(Requirement.MatchType.EQUALS)
                                        .matchValue("Linux"))
                                .tasks(new VcsCheckoutTask()
                                                .description("Checkout Default Repository")
                                                .checkoutItems(new CheckoutItem().defaultRepository()
                                                                .path("bamboo"),
                                                        new CheckoutItem()
                                                                .repository(new VcsRepositoryIdentifier()
                                                                        .name("After deployment trigger"))
                                                                .path("plugin")),
                                        new ScriptTask()
                                                .description("Install Firefox")
                                                .inlineBody("#!/bin/sh\n\nsudo apt-get -y -q --no-install-recommends install firefox libasound2\n"),
                                        new MavenTask()
                                                .description("Build Bamboo")
                                                .goal("${bamboo.bambooQuickCompileMavenGoals}")
                                                .environmentVariables("MAVEN_OPTS=\"${bamboo.funcMavenOpts}\"")
                                                .jdk("JDK 1.8")
                                                .executableLabel("Maven 3.2")
                                                .workingSubdirectory("bamboo"),
                                        new AnyTask(new AtlassianModule("com.atlassian.bamboo.plugins.variable.updater.variable-updater-generic:variable-extractor"))
                                                .configuration(new MapBuilder<String, String>()
                                                        .put("overrideCustomised", "")
                                                        .put("branchVars", "")
                                                        .put("variable", "bambooVersion")
                                                        .put("removeSnapshot", "")
                                                        .put("includeGlobals", "")
                                                        .put("variableScope", "JOB")
                                                        .put("pomFile", "bamboo/pom.xml")
                                                        .build()),
                                        new MavenTask()
                                                .description("test plugin against Bamboo SNAPSHOT")
                                                .goal("clean install ${bamboo.mavenArgs} -Dbamboo.version=${bamboo.bambooVersion}")
                                                .environmentVariables("MAVEN_OPTS=\"${bamboo.funcMavenOpts}\"")
                                                .jdk("JDK 1.8")
                                                .executableLabel("Maven 3.2")
                                                .hasTests(true)
                                                .testResultsPath("**/surefire-reports/*.xml")
                                                .workingSubdirectory("plugin"))
                                .requirements(new Requirement("selenium")
                                        .matchValue("true")
                                        .matchType(Requirement.MatchType.EQUALS))))
                .linkedRepositories("Bamboo Master on Stash")
                .planRepositories(new BitbucketCloudRepository()
                        .name("After deployment trigger")
                        .repositoryViewer(new BitbucketCloudRepositoryViewer())
                        .repositorySlug("atlassianlabs", "bamboo-after-deployment-trigger")
                        .accountAuthentication(new SharedCredentialsIdentifier("Bamboo BBC Account"))
                        .checkoutAuthentication(new SharedCredentialsIdentifier("SSH access to bitbucket.org"))
                        .branch("master")
                        .changeDetection(new VcsChangeDetection()))

                .triggers(new RepositoryPollingTrigger()
                                .triggeringRepositoriesType(RepositoryBasedTrigger.TriggeringRepositoriesType.SELECTED)
                                .selectedTriggeringRepositories(new VcsRepositoryIdentifier()
                                        .name("After deployment trigger"))
                                .withPollingPeriod(Duration.ofSeconds(300)),
                        new AnyTrigger(new AtlassianModule("com.atlassian.bamboo.triggers.atlassian-bamboo-triggers:daily"))
                                .name("Single daily build")
                                .configuration(new MapBuilder<String, String>()
                                        .put("trigger.created.by.user", "achystoprudov")
                                        .put("repository.change.daily.buildTime", "03:00")
                                        .build()))
                .planBranchManagement(new PlanBranchManagement()
                        .delete(new BranchCleanup())
                        .notificationForCommitters())
                .notifications(new Notification()
                        .type(new PlanStatusChangedNotification())
                        .recipients(new UserRecipient("achystoprudov")));
    }

    public static void main(String... argv) {
        //By default credentials are read from the '.credentials' file.
        BambooServer bambooServer = new BambooServer("https://tardigrade-bamboo.internal.atlassian.com");
        final PlanSpec planSpec = new PlanSpec();

        final Plan plan = planSpec.plan();
        bambooServer.publish(plan);
    }
}